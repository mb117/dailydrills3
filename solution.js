const items = require("./3-arrays-vitamins.js");
const _ = require("lodash"); 

// 1. Get all items that are available 
function getAvailableItems(items) {
    let result = items.filter(function(item) {
        return item.available;
    });

    return result;
}

// console.log(getAvailableItems(items));

// 2. Get all items containing only Vitamin C.
function getVitaminC(items) {
    let result = items.filter(function(item) {
        return item.contains.split(",").length === 1 && item.contains.includes("Vitamin C");
    });

    return result;
}

// console.log(getVitaminC(items));


// 3. Get all items containing Vitamin A.
function getVitaminA(items) {
    let result = items.filter(function(item) {
        return item.contains.includes("Vitamin A");
    });
    
    return result;
}

// console.log(getVitaminA(items));

/*
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
*/ 
/*
[{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
},
*/

function groupVitamins(items) {
    
    let uniqueVitamins = items.map(function(item) {
        return item.contains.split(",");
    })
    .flat(Infinity)
    .map(function(item) {
        return item.trim();
    })
    .reduce(function(uniqueVitamins, vitamin) {
        uniqueVitamins[vitamin] = [];

        return uniqueVitamins;
    }, {});
    

    console.log(uniqueVitamins);
}

console.log(groupVitamins(items));

// 5. Sort items based on number of Vitamins they contain.
function sortByNumberOfVitamins(items) {
    let sortedItems = [];

    // adding a new property called numberOfVitamins
    sortedItems = items.map(function(item) {
        item["numberOfVitamins"] = item.contains.split(",").length;

        return item;
    });

    sortedItems = _.orderBy(sortedItems, ["numberOfVitamins"], ["asc"]);

    return sortedItems;
}

// console.log(sortByNumberOfVitamins(items));
